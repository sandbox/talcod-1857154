<div id="og_members">
  <script type="text/javascript">
    og_member_url = Drupal.settings.basePath + 'node/'+<?php print $node->nid; ?>
  </script>
  <?php foreach ($members as $id => $data): ?>
    <div id="og_members_<?php print $id; ?>" class="og_member_status">
      <div class="og_member_title"><?php print $data['name']; ?></div>
      <?php foreach ($data['members'] as $user): ?>
        <div id="og_member_element_<?php print $user->uid; ?>" class="og_member_elt">
          <?php print $user->name; ?>
        </div>
      <?php endforeach; ?>
    </div>
  <?php endforeach; ?>
</div>
