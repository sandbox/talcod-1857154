/*
 * TALA
 * Copyright (c) 2012 TALCOD SAS and by respective authors (see below).
 * http://www.talcod.net
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along 
 * with this program; if not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * $Id: tala.js,v 0.9.1 2012/11/30 15:12:00 $
 */

/**
 * @file 
 * TALA - Improve governance for enterprises.
 *
 * @author
 * Grégory Nutte <gregory.nutte@talcod.net>
 */

/**
 * Allow field edition on node view page
 */
Drupal.behaviors.tala = function(context) {
  $('fieldset.tala-editable', context).click(function() {
    if($(this).hasClass('tala-editable')) {
      var object = $(this);
      var id = $(this).id;
      var url = Drupal.settings.basePath + 'tala/fields/edit/' + Drupal.settings.tala.nid + '/' + id;
    
      object.removeClass('tala-editable');
    
      $.ajax({
        url: url,
        type: 'GET',
        success: function(response) {
          object.html(response.data);
        },
        error: function() {
                
        },
        dataType: 'json'
      });
    }
  });
};