<div class="inner clearfix">
  <h2 class="title block-title">Proposition d'ordre du jour</h2>

  <div class="content">
    <div class="">
      <?php print l(t('Add point to the paper order'), "tala/" . $instance->nid . "/point/add"); ?>
    </div>

    <?php if (count($paper_order) == 0): ?>
      Il n'y aucun point à l'ordre du jour ajouté.
    <?php else: ?>
      <?php foreach ($paper_order as $pid => $point): ?>
        <h3><?php print l($point->title, "tala/" . $instance->nid . "/point/edit/" . $pid); ?></h3>
        <div class="content">
          <div class="field-label"><?php print t('Point reporting'); ?></div>
          <div class="field_content"><?php print $point->content ?></div>
        </div>
      <?php endforeach; ?>
    <?php endif; ?>
  </div>
</div>