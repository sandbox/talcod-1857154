<?php

/*
 * TALA
 * Copyright (c) 2012 TALCOD SAS and by respective authors (see below).
 * http://www.talcod.net
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along 
 * with this program; if not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * $Id: tala_archive.theme.inc,v 0.9.1 2012/11/30 15:12:00 $
 */

/**
 * @file 
 * TALA - Improve governance for enterprises.
 *
 * @author
 * Grégory Nutte <gregory.nutte@talcod.net>
 */

/**
 * Show child links for a node
 * @param type $node
 * @return string 
 */
function theme_archive_new_child_links($node) {
  $out = "";
  $create_links = array();

  if (user_access('create child nodes') && (user_access('create child of any parent') || node_access('update', $node))) {
    foreach (nodehierarchy_get_allowed_child_types($node->type) as $key) {
      if (node_access('create', $key)) {
        $type_name = node_get_types('name', $key);
        $destination = drupal_get_destination() . "&parent=$node->nid";
        $key = str_replace('_', '-', $key);
        $title = t('Add a new %s.', array('%s' => $type_name));
        $create_links[] = l($type_name, "node/add/$key", array('query' => $destination, 'attributes' => array('title' => $title)));
      }
    }
    if ($create_links) {
      $out = '<div class="newchild">' . t("Create new child !s", array('!s' => implode(" | ", $create_links))) . '</div>';
    }
  }
  return $out;
}

/**
 * Childs form
 * @param type $form
 * @return type 
 */
function theme_archive_children_form($form) {
  drupal_add_tabledrag('children-list', 'order', 'sibling', 'menu-weight');

  $colspan = module_exists('nodeaccess') ? '4' : '3';

  $header = array(
    t('Title'),
    t('Type'),
    t('Weight'),
    array('data' => t('Operations'), 'colspan' => $colspan),
  );

  $rows = array();
  foreach (element_children($form['children']) as $mlid) {
    $element = &$form['children'][$mlid];

    // Add special classes to be used for tabledrag.js.
    $element['weight']['#attributes']['class'] = 'menu-weight';

    $node = $element['node']['#value'];
    $row = array();
    $row[] = drupal_render($element['title']);
    $row[] = drupal_render($element['type']);
    $row[] = drupal_render($element['weight']);
    $row[] = node_access('update', $node) ? l(t('edit'), 'node/' . $node->nid . '/edit') : '';
    $row[] = node_access('delete', $node) ? l(t('delete'), 'node/' . $node->nid . '/delete') : '';
    if (module_exists('nodeaccess')) {
      $row[] = nodeaccess_access('grant', $node) ? l(t('grant'), 'node/' . $node->nid . '/grant') : '';
    }

    $row = array('data' => $row);
    $row['class'] = !empty($row['class']) ? $row['class'] . ' draggable' : 'draggable';
    $rows[] = $row;
  }
  $output = '';
  if ($rows) {
    $output .= theme('table', $header, $rows, array('id' => 'children-list'));
  }
  $output .= drupal_render($form);
  return $output;
}

?>
