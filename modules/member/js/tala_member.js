/*
 * TALA
 * Copyright (c) 2012 TALCOD SAS and by respective authors (see below).
 * http://www.talcod.net
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along 
 * with this program; if not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * $Id: tala_member.js,v 0.9.1 2012/11/30 15:12:00 $
 */

/**
 * @file 
 * TALA - Improve governance for enterprises.
 *
 * @author
 * Grégory Nutte <gregory.nutte@talcod.net>
 */

var og_member_drag_element = null;
var og_member_url = null;
var move_it = jQuery('<span id="info-deplacement">Relachez la souris pour déplacer ici</span>');
var state = 0;

$(function() {
  jQuery('div.og_member_elt').mousedown(function(event){
    state = 1;
    og_member_drag_element = this;
    //$(this).hide();
    $(this).css('opacity','0.2'); 
        
  
    jQuery('div.og_member_status').hover(function(){
      if(state == 1)  {
        $(this).css('border-color','#ccc');
        $(this).append(move_it);
      }

    });
   
    jQuery('div.og_member_status').mouseout(function(){
      $(this).css('border-color','#003375');  
      move_it.remove();
    });
      
  
   
  });
    
  jQuery('div.og_member_status').mouseup(function(){  
    state = 0;
    $(this).append(og_member_drag_element);
    $(og_member_drag_element).show().css('opacity','1');
        
    var id = og_member_drag_element.id;
    var uid = id.substr(id.lastIndexOf('_')+1, id.length);
        
    id = this.id;
    var sid = id.substr(id.lastIndexOf('_')+1, id.length);
        
    var url = og_member_url+'/members/'+uid+'/'+sid;
        
    $.ajax({
      url: url,
      type: 'GET',
      //data: nid,
      success: function(response) {
                
      },
      error: function() {
        alert(Drupal.t("An error occurred at @path.", {
          '@path': url
        }));
      },
      dataType: 'json'
    });
        
    og_member_drag_element = null;
  });
});