<div class="content">
  <?php if (count($docs) == 0): ?>
    Il n'y a pas encore de document ajouté.
  <?php else: ?>
    <?php foreach ($docs as $did => $doc): ?>
      <div class="content">
        <div class="field_content"><?php print l($doc->title, "node/" . $did); ?></div>
      </div>
    <?php endforeach; ?>
  <?php endif; ?>

  <div class="">
    <?php print l(t('[add]'), "tala/" . $instance->nid . "/doc/add"); ?>
    <?php print l(t('[show all]'), "tala/" . $instance->nid . "/doc/list"); ?>
  </div>
</div>