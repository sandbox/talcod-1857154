<?php

/*
 * TALA
 * Copyright (c) 2012 TALCOD SAS and by respective authors (see below).
 * http://www.talcod.net
 * 
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU Lesser General Public License along 
 * with this program; if not, see <http://www.gnu.org/licenses/>.
 * 
 * 
 * $Id: tala.fieldgroup.inc,v 0.9.1 2012/11/30 15:12:00 $
 */

/**
 * @file 
 * TALA - Improve governance for enterprises.
 *
 * @author
 * Grégory Nutte <gregory.nutte@talcod.net>
 */

/**
 * Provide group field description for "instance" node type
 * @return string 
 */
function tala_fieldgroup_default_groups() {
  $groups = array();

  // Exported group: group_parametres_instance
  $groups['instance-group_parametres_instance'] = array(
    'group_type' => 'standard',
    'type_name' => 'instance',
    'group_name' => 'group_instance_parameter',
    'label' => 'Instance parameter',
    'settings' => array(
      'form' => array(
        'style' => 'fieldset',
        'description' => '',
      ),
      'display' => array(
        'description' => '',
        'label' => 'above',
        'teaser' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'full' => array(
          'format' => 'hidden',
          'exclude' => 0,
        ),
        '4' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '2' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '3' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        '5' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
        'token' => array(
          'format' => 'fieldset',
          'exclude' => 0,
        ),
      ),
    ),
    'weight' => '-4',
    'fields' => array(
      '0' => 'field_structure_name',
      '1' => 'field_structure_type',
      '2' => 'field_president',
      '3' => 'field_secretaire',
      '4' => 'field_commissaire',
      '5' => 'field_scrutateur1',
      '6' => 'field_scrutateur2',
      '7' => 'field_quorum',
    ),
  );

  t('Instance parameter');

  return $groups;
}

?>
